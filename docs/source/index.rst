.. JSONStreamWriter documentation master file, created by
   sphinx-quickstart on Tue May 18 10:00:34 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

JSONAutoArray
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: json_autoarray
   :members:

.. automodule:: json_autoarray.JSONAutoArray
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
